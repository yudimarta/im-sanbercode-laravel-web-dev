<?php
    require_once './animal.php';
    require_once './frog.php';
    require_once './ape.php';

    $name_field = "Name: ";
    $legs_field = "legs: ";
    $cold_blooded_field = "cold blooded: ";

    $sheep = new Animal("shaun");

    echo $name_field . $sheep->get_name() . "<br>";
    echo $legs_field . $sheep->get_legs() . "<br>";
    echo $cold_blooded_field . $sheep->get_cold_blooded() . "<br>";

    $kodok = new Frog("buduk");
    echo $name_field . $kodok->get_name() . "<br>";
    echo $legs_field . $kodok->get_legs() . "<br>";
    echo $cold_blooded_field . $kodok->get_cold_blooded() . "<br>";
    echo "jump: " . $kodok->jump() . "<br>";

    $sungokong = new Ape("kera sakti");
    echo $name_field . $sungokong->get_name() . "<br>";
    echo $legs_field . $sungokong->get_legs() . "<br>";
    echo $cold_blooded_field . $sungokong->get_cold_blooded() . "<br>";
    echo "Yell: " . $sungokong->yell();
?>